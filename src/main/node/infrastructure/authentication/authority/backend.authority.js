//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const util = require('util');
const axios = require('axios');

const AbstractAuthority = require('../abstract.authority');

class BackendAuthority extends AbstractAuthority {

  /**
   * Validate that the authenticated user is authorized to access the request API with a backend service.
   * The expect behavior is a that: a successfull (2xx) response indicate that the user is authorized to 
   * access this API; any error from the upstream service indicate that the user is not authorize to access 
   * this API.
   * @param {Profile} profile the profile provided by the authorizations step
   * @param {Any} configuration the aonfiguration provided by the user for this authority.
   * @returns {Promise<Any>} a resolved promise
   * 
   * @implements AbstractIdentityValidator#validate(profile);
   * @public
   */
  validate(profile, configuration) {
    let body = undefined;
    let method = (configuration.method || 'get').toLowerCase();
    let endpoint = util.format(configuration.urlFormat, Object.assign({}, profile, configuration));
    if (method == 'post') {
      body = util.format(configuration.bodyFormat, Object.assign({}, profile, configuration));
    }
    return axios.request({ url: endpoint, method: method, data: body });
  }
}

/**
 * Validate that the authenticated user is authorized to access the request API with a backend service
 * @public
 * @typedef BackendAuthority
 */
module.exports = BackendAuthority;