//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */
/* global setTimeout */

const Component = require('dino-core').Component;

class AbstractDiscoveryService extends Component {

  constructor() {
    super();

    /**
     * @type {Any}
     * @private
     */
    this.backend = undefined;
    /**
     * @type {Array<Instace>}
     * @private
     */
    this.instances = undefined;
  }

  /**
   * Configure this discovery service to discover instances of the 
   * requested backend.
   * @param {Any} backend the backend configuration, definition varies 
   * depending on the backend to support
   * 
   * @public
   */
  configure(backend) {
    this.validate(backend);
    this.backend = backend;
  }

  /**
   * Discover all the reachable instances of the requested service.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support
   * @returns {Promise<Array<Service>>} All discovered instances
   * 
   * @public
   */
  async discover() {
    if (!this.instances) {
      this.instances = await this.doDiscover(this.backend);
      if (this.backend.watch) {
        setTimeout((async () => this.instances = await this.doDiscover(this.backend)).bind(this), 30000);
      }
    }
    return this.instances;
  }

  /**
   * Discover all the reachable instances of the requested service.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support
   * @returns {Array<Service>} All discovered instances
   * 
   * @protected
   */
  doDiscover(backend) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }

  /**
   * Validate the configuration provided is as expected for the requested backend.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support 
   * @throws {Error} if the validation is not successful
   * 
   * @protected
   */
  validate(backend) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }

  static scope() {
    return 'transient';
  }

}

/**
 * Abstract discovery service, define the interface of a discovery service.
 * @public
 * @abstract
 * @typedef AbstractDiscoveryService
 */
module.exports = AbstractDiscoveryService;