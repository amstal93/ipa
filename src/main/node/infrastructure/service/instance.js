//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */
/* global process */

const url = require('url');
const axios = require('axios');

const logger = require('../logger');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

class Instance {

  /**
   * Create a new Instance
   * @param {String} id the instance identifier
   * @param {String} protocol the protocol to use for commnunicating with the intance
   * @param {String} host the DNS or IP Address the instance is reachable at
   * @param {Number} port the port the instance is reachable at
   * @param {String} path the base path section of the target URL
   * 
   * @public
   */
  constructor(id, protocol, host, port, basePath) {
    this.id = id;
    this.protocol = protocol;
    this.host = host;
    this.port = port;
    this.basePath = basePath;
  }

  /**
   * Invoke this instance with the client request.
   * @param {Any} req the client request
   * @returns {Promise} a promise resolved when the response from the server is received
   * 
   * @public
   */
  invoke(req) {
    return axios.request(this.buildRequestOptions(req));
  }

  buildRequestOptions(req) {
    let pathname = `${this.basePath}${req.path}`.replace('//', '/');
    let responseType = this.determineResponseType(req);
    let answer = {
      url: url.format({ protocol: this.protocol, hostname: this.host, port: this.port, pathname: pathname, query: req.query }),
      method: req.method.toLowerCase(),
      headers: Object.entries(req.headers).reduce((acc, header) => {
        let name = header[0];
        if (!['host', 'authorization'].includes(name.toLowerCase())) {
          acc[header[0]] = header[1];
        }
        return acc;
      }, {}),
      // https://gitlab.com/codesketch/ipa/issues/5
      // as a defuault set responseType as arraybuffer so the we get the result as is,
      // see https://github.com/axios/axios/blob/ccc78899bb6f595e3c44ec7ad6af610455859d78/lib/adapters/http.js#L197
      // that means the response can be trated as utf8 string only if needed meaning that the response is json, xml, etc. 
      responseType: responseType
    };
    if (['post', 'put', 'patch'].includes(req.method.toLowerCase())) {
      answer.data = req.body;
    }
    logger.debug('request options', JSON.stringify(answer));
    return answer;
  }

  determineResponseType(req) {
    let answer = 'arraybuffer';
    if (req.headers.accept) {
      const match = /.+\/(.+)/gi.exec(req.headers.accept);
      if (['javascript', 'json', 'plain', 'csv', 'xml', 'css', 'html',].some(st => match[1] == st)) {
        answer = match[1];
      }
    }
    return answer;
  }
}

/**
 * An instance of a service
 * @public
 * @typedef Instance
 */
module.exports = Instance;